# coding=utf-8
'''
Copyright 2015 Diego Ariel Capeletti

Este archivo es parte del proyecto (((Dosificador alimenticio libre | Free food dispenser))).

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''

'''
Created on 09/10/2015

@author: diegoariel
'''
import RPi.GPIO as GPIO
import time
import threading
from observable import Observable 

class SensorProximidad(threading.Thread, Observable):
    def __init__(self, pin_trig, pin_echo, mensaje):
        threading.Thread.__init__(self)
        Observable.__init__(self)
        self.pin_trig = pin_trig #Pin encargado de escuchar el rebote
        self.pin_echo = pin_echo #Pin encargado de enviar un pulso microsonoro
        self.mensaje_notificacion = mensaje #Mensaje que se envía al mediador ante eventos del sensor.
        self.daemon=True
        self.paused=True
        self.state=threading.Condition()
        if pin_trig != 0 and pin_echo !=0: #Inicializo el sensor solo si los pines son <> de 0 
            self.inicializar_sensor()
        
        
    def run(self):
        #self.resume() Al hacer start() no lo inicio al thread, solo cuando se hace resume
        while True:
            with self.state:
                if self.paused: #Se evalua el estado del hilo, si esta pausado, lo pongo en espera...
                    self.state.wait() # block until notified #bloqeuar hasta que se notifica
            
            GPIO.output(self.pin_trig, False)    
            print "Esperando respuesta"
            time.sleep(2)

            GPIO.output(self.pin_trig, True)
            time.sleep(0.00001)
            GPIO.output(self.pin_trig, False)

            while GPIO.input(self.pin_echo) ==0:
                pulse_start = time.time()

            while GPIO.input(self.pin_echo)==1:
                pulse_end=time.time()
            
            pulse_duration= pulse_end-pulse_start
            
            distance = pulse_duration * 17150
                
            distance = round(distance, 2)
            print distance
            #print "Distancia: ", distance, " cm"
            mensaje = "<"+ self.mensaje_notificacion +">" + str(distance) + "</"+ self.mensaje_notificacion +">\n" #La distancia SIEMPRE es en cm...
            self.notify_observers(mensaje)
            self.pause() #Pongo en pausa xq no quiero notificar de nuevo
            
       
        
    def resume(self):
        #Pone en marcha la ejecución del thread
        with self.state:
            self.paused = False
            self.state.notify()  # unblock self if waiting #desbloquear auto esperando

    def pause(self):
        #Para la ejecución del thread
        with self.state:
            self.paused = True  #El hilo deja de ejecutarse, entra en estado de espera..
            
    
    def inicializar_sensor(self):
        GPIO.setmode(GPIO.BCM)
        #print "Medicion de distancia en progreso.."
        GPIO.setup(self.pin_trig, GPIO.OUT)
        GPIO.setup(self.pin_echo, GPIO.IN)
    
    def detener_y_borrar(self):
        GPIO.cleanup()
        self.pause()
        
    def setPin_trig(self, pin_trig):
        self.pin_trig = pin_trig
        
    def getPin_trig(self):
        return self.pin_trig
    
    def setPin_echo(self, pin_echo):
        self.pin_echo = pin_echo
        
    def getPin_echo(self):
        return self.pin_echo
        
        
  