# coding=utf-8
'''
Copyright 2015 Diego Ariel Capeletti

Este archivo es parte del proyecto (((Dosificador alimenticio libre | Free food dispenser))).

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''
'''
Esta clase provee el servicio de comunicación por email al programa servidor.
Esta clase deberá almacenar los mensajes que pueden provenir de eventos del hardware
y enviar a la dirección o direcciones que correspondan...

Si el servidor no tiene conexión a internet, esta clase no estará en servicio...
'''

import threading
from observable import Observable
import smtplib
import time

class ServiceEmail(threading.Thread, Observable):
    def __init__(self, server_smtp, server_port, dir_email, contrasena):
        threading.Thread.__init__(self)
        Observable.__init__(self)        
        self.daemon=True
        self.paused=True
        self.state=threading.Condition()
        
        self.__nombre_server_smtp = server_smtp #"smtp.gmail.com" #smtp.openmailbox.org
        self.__server_port = server_port #587
        self.__direccion_email = dir_email #Su direccion de email que tiene que concordar con el dominio...
        self.__password = contrasena #Contraseña de su email
        
        #--------CONEXION AL SERVIDOR-------------
        try:
            self.__server = smtplib.SMTP(self.__nombre_server_smtp, self.__server_port)
            self.__server.ehlo()
            self.__server.starttls()
            self.__server.ehlo()
            self.__server.login(self.__direccion_email, self.__password)
        except smtplib.SMTPAuthenticationError as error:
            print "Error al enviar el email. Revice su dirección de correo y contraseña. ", error
        except smtplib.SMTPRecipientsRefused as error:
            print "Error al enviar el email. ", error
        except smtplib.SMTPServerDisconnected as error:
            print "Error al intentar conectarse al servidor de email. ", error 
        except Exception as error:
            print "Error deconocido. ", error
        #----------------------------------------
        
        self.__mensajes_email = [] #Tupla que almacena el email que se enviará...Cuando se envía, se elimina de la cola de mensajes...
        self.__destinatario=[] #Tupla de destinatarios     
        
        
    def run(self):
        self.resume()
        while True:
            with self.state:
                if self.paused: #Se evalua el estado del hilo, si esta pausado, lo pongo en espera...
                    self.state.wait() # block until notified #bloqeuar hasta que se notifica
            if len(self.__mensajes_email) != 0:#Hay un email para enviar...
                datos_email = self.__mensajes_email[0:1] #Tomo los valores en el órden que se ingresados...
                mensaje = "Subject: " + datos_email[0][1] + "\n" + datos_email[0][2]
                #self.enviar_email(datos_email[0][0], mensaje)
                self.__server.sendmail(self.__destinatario, datos_email[0][0], mensaje)
                del self.__mensajes_email[0:1] #Una vez enviado, lo elimino de la cola de emails....
                print "Email enviado correctamente..."
            
            time.sleep(1)
 
    
    def resume(self):
        #Pone en marcha la ejecución del thread
        with self.state:
            self.paused = False
            self.state.notify()  # unblock self if waiting #desbloquear auto esperando

    def pause(self):
        #Para la ejecución del thread
        with self.state:
            self.paused = True  # make self block and wait #El hilo deja de ejecutarse, entra en estado de espera..

        
    def enviar_email(self, destinatario, asunto, mensaje):
        self.__mensajes_email.append([destinatario, asunto, mensaje])
        
    '''    
    def send_email(self, destinatario, mensaje):
        try:
            #__server = smtplib.SMTP(self.__nombre_server_smtp, self.__server_port) #¿Por cada email tiene que haber autenticacion?
            #__server.ehlo()
            #__server.starttls()
            #__server.ehlo()
            #-------------------NO VA--------------
            #self.__direccion_email = "d.a.capeletti@gmail.com" #Su email tiene que pertenecer al dominio...
            #my_password = "capeletti33426000"
            #destination = "d.a.capeletti@openmailbox.org"
            #text = "Alguien ha presionado el boton! Las vacas estan balando MUUUUUU"
            #--------------------------------------
            #__server.login(self.__direccion_email, self.__password)
            self.__server.sendmail(self.__direccion_email, destinatario, mensaje)
            #__server.quit()
            print("El aviso ha sido enviado correctamente!")
        except smtplib.SMTPAuthenticationError as error:
            print "Error al enviar el email. Revice su dirección de correo y contraseña. ", error
        except smtplib.SMTPRecipientsRefused as error:
            print "Error al enviar el email. ", error
        except smtplib.SMTPServerDisconnected as error:
            print "Error al intentar conectarse al servidor de email. ", error
            
    '''
        
    def setDireccionEmail(self, email):
        self.__direccion_email = email
        
    def getDireccionEmail(self):
        return self.__direccion_email
    
    def setPuertoSMTP(self, puerto):
        self.__server_port = puerto
        
    def getPuertoSMTP(self):
        return self.__nombre_server_smtp
    
    def setPassword(self, password):
        self.__password = password
        
    def getPassword(self):
        return self.__password
    
    def setSMTPServer(self, server):
        self.__nombre_server_smtp = server
        
    def getSMTPServer(self):
        return self.__nombre_server_smtp
    
    def desconectarEmail(self):
        self.__server.quit()
        
    def agregarDestinatario(self, email_destinatario):
        self.__destinatario.append(email_destinatario)
    
    
        
        
        
        