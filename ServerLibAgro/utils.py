# coding=utf-8
'''
Copyright 2015 Diego Ariel Capeletti

Este archivo es parte del proyecto (((Dosificador alimenticio libre | Free food dispenser))).

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''
'''
Created on 29/01/2016

@author: diegoariel
'''
import subprocess

def verificarPaqueteInstalado(paquete):
    #Verifica si un paquete esta instalado...
    process = subprocess.Popen("dpkg -l | grep "+ paquete, stdout=subprocess.PIPE, stderr=None, shell=True)

    #Lanzo el comando...
    output = process.communicate() #La salida es una tupla....
    
    if paquete in output[0]:
        return True
        #print "Esta isntalado..."
    else:
        return False
        #print "No esta instalado..."

        
def instalarPaquete(paquete):
    #Instala un paquete. Primero actualizamos la lista de paquetes y luego lo instalamos.
    import getpass
    password = getpass.getpass() #Obtengo el password
    proc = subprocess.Popen('echo '+ password +' | sudo -S apt-get update', shell=True, stdin=None, stdout=open("/dev/null", "w"), stderr=None, executable="/bin/bash")
    proc.wait() #Con wait detiene la GUI. Debería haber un thead???
    
    proc = subprocess.Popen('echo '+ password +' | sudo -S apt-get -y install ' + paquete, shell=True, stdin=None, stdout=open("/dev/null", "w"), stderr=None, executable="/bin/bash")
    proc.wait()
    if verificarPaqueteInstalado(paquete) == True:
        print "Paquete instalado con éxito..."
    else:
        print "Hubo un problema al instalar el paquete..."     
        
        
    
        