# coding=utf-8
'''
Copyright 2015 Diego Ariel Capeletti

Las clases que quieren escuchar a los eventos de sensores/actuadores, deben heredar de esta 
clase. Esta clase con Observable forman parte del patrón observer.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''
'''
Created on 08/10/2015

@author: diegoariel
'''

class Observer():
    def update(self, observable, event):
        raise NotImplemented('Este método es abstracto. Debe sobrescribirlo.')