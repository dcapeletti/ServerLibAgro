# coding=utf-8
'''
Copyright 2015 Diego Ariel Capeletti

Este archivo es parte del proyecto (((Dosificador alimenticio libre | Free food dispenser))).

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''
'''
Created on 17/11/2015

@author: diegoariel
'''

from xml.dom import minidom

class ConfiguracionXML:
    def __init__(self, archivo):
        self.dom = minidom.parse(archivo) #Le pasamos el archivo y tenemos el DOM
        
    def getDOM(self):
        return self.dom
    
    def getNombreServidorBD(self):
        for n in self.dom.getElementsByTagName("servidorBD"):
            return n.toxml() #Imprime el nombre del nodo con su valor...<nombre>pepe</nombre>
            #return n.firstChild.data #Solo imprime el valor del nodo: pepe
        
    def getValorNombreServidor(self):
        for n in self.dom.getElementsByTagName("servidorBD"):
            return n.firstChild.data #Solo devuelve el valor
        
        
    def getNombreBD(self):
        for n in self.dom.getElementsByTagName("nombreBD"):
            return n.toxml() #Imprime el nombre del nodo con su valor...<nombre>pepe</nombre>
            #return n.firstChild.data #Solo imprime el valor del nodo: pepe
            
    def getValorNombreBD(self):
        for n in self.dom.getElementsByTagName("nombreBD"):
            return n.firstChild.data #Solo imprime el valor del nodo: pepe
        
        
    def actualizarConfiguracion(self):
        self.dom = minidom.parse("./Config.xml") #Le pasamos el archivo y tenemos el DOM
    
    
    
    
    
    
    
    
    
    
    
    