# coding=utf-8
'''
Copyright 2015 Diego Ariel Capeletti

Este archivo es parte del proyecto (((Dosificador alimenticio libre | Free food dispenser))).

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''
'''
Created on 18/12/2015

@author: diegoariel
'''

#from servomotor import Servo
#from rele import Rele
#from controlbutton import Boton

class Compuesto():
    def add(self, Compuesto):
        raise NotImplementedError("Debe implementar este método.")
    
    def remove(self, Compuesto):
        raise NotImplementedError("Debe implementar este método.")
        
    def operacion(self):
        raise NotImplementedError("Debe implementar este método.")
    
    def setParent(self, Compuesto):
        raise NotImplementedError("Debe implementar este método.")
    
    def getParent(self):
        raise NotImplementedError("Debe implementar este método.")
    
    
    '''La clase Compuerta define el modelo de objetos que la componen: Patrón Composite. 
    Cuando un objeto Boton lanza un evento, se puede saber a que compuerta pertenece 
    y a la vez se puede enviar el comando de apertura al motor porque la compuerta 
    también conoce el motor...
    Podemos decir que una compuerta esta compuesta por:
    * Boton de pulsación de apertura.
    * Motor para apertura.
    * Rele para control del motor y vibrador.
    '''
class Compuerta(Compuesto):
    def __init__(self, identificador):
        self.__identificador_compuerta = identificador
        self.__componentes = {}
    
    def add(self, Compuesto):
        #Agrego el compuesto sumando +1 a la última clave del índice del diccionario...
        if len(self.__componentes) ==0:
            Compuesto.setParent(self)
            self.__componentes[0] = Compuesto
        else:
            Compuesto.setParent(self)
            self.__componentes[self.__componentes.keys()[-1]+1] = Compuesto
    
    def remove(self, Compuesto):
        #Buscar el componente y si coincide, eliminarlo del diccionario...
        #del self.__componentes
        pass
    
    def operacion(self):
        #No hago nada por el momento...
        pass
    
    def getComponentes(self):
        return self.__componentes
    
    def getMotor(self):
        #Busco el servo motor en la lista de componentes...
        for pos_obj in self.__componentes: #Devuelve el primer motor que se encuentra...
            if "Servo" in str(self.__componentes[pos_obj].__class__) or "MotorSinThreads" in str(self.__componentes[pos_obj].__class__) or "MotorConThreads" in str(self.__componentes[pos_obj].__class__):
                return self.__componentes[pos_obj]
    
    def getRele(self):
        #Busco el rele en la lista de componentes...
        for pos_obj in self.__componentes: #Devuelve el primer rele que se encuentra...
            if "Rele" in str(self.__componentes[pos_obj].__class__):
                return self.__componentes[pos_obj]
            
    def getBotonCompuerta(self):
        #Busco el Boton en la lista de componentes...
        for pos_obj in self.__componentes: #Devuelve el primer boton que se encuentra...
            if "Boton" in str(self.__componentes[pos_obj].__class__):
                return self.__componentes[pos_obj]
        
            
    def getIdCompuerta(self):
        return self.__identificador_compuerta