# coding=utf-8
'''
Copyright 2015 Diego Ariel Capeletti

Este archivo es parte del proyecto (((Dosificador alimenticio libre | Free food dispenser))).

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''
'''
Created on 17/11/2015

@author: diegoariel
'''
#Para distros debian y derivados: sudo apt-get install python-mysqldb
'''Esta clase provee el servicio a la BD para el programa servidor.
Permite guardar registros de manera asincrona para los eventos de la máquina. Esto garantiza que no se pierden 
registros al producirse eventos del hardware.
Esta clase gestiona tuplas que almacenan eventos de los componentes de la máquina. Las tuplas se tratan como colas 
de eventos. Los eventos que se reciben se guardan en cada cola y se procesan por órden de llegada (FIFO). Al procesar
cada evento de las colas, estos se eliminan de las mismas. Cuando la cola ya no tiene eventos, todos los mensajes se
han almacenado en la BD.
'''
import time
import threading
import sys
try: #Intento importar el paquete para acceso a bases de datos
    import MySQLdb
    from MySQLdb import OperationalError
except ImportError as error:
    print "El paquete python-mysqldb no está instalado. Lo instalaremos para poder continuar de lo contrario presione Ctrl+c para cancelar..."
    import utils
    if utils.verificarPaqueteInstalado("python-mysqldb") == False:
        utils.instalarPaquete("python-mysqldb")
        print "Por favor, inicie de nuevo el programa.."
        sys.exit()


class ServiceBD(threading.Thread):
    def __init__(self, serverBD, nombreBD, usuario, password):
        threading.Thread.__init__(self)
        self.daemon = True  # OK for main to exit even if instance is still running #permite salir aunque el main se este ejecutando...
        self.paused = True  # Comienzo en pausa
        self.state = threading.Condition() #Devuelve un objeto de condición variable. Permite que uno o mas hilos esperen hasta que sean notificados...
        
        self.__serverBD = serverBD
        self.__nombreBD = nombreBD
        self.__usuario = usuario
        self.__password = password
        
        try:
            self.__bd = MySQLdb.connect(host=serverBD, user=usuario, passwd=password, db=nombreBD)
            self.__bd.ping(True) #reconexion automatica si es que se pierde....
        except Exception as error:
            print error 
        self.__eventos_tapa = [] #Tupla o cola de eventos...Se llena cuando hay evento y se elimina cuando se procesan...
        self.__eventos_motor_compuerta = [] #Tupla o cola de eventos del motor que acciona la compuerta...
        self.__eventos_conexiones_red = [] #Tupla o cola de eventos de red...
        self.__eventos_sensor_alimento = [] #Tupla de eventos del sensor de alimento...
        self.__eventos_tarea_programada = []
        self.__tiempo_actualizacion = 500 #Cada vez que el thread cuenta hasta 500, hace una lectura a la BD
        self.__contador = 0

    
    def run(self):
        self.resume()
        while True:
            with self.state:
                if self.paused: #Se evalua el estado del hilo, si esta pausado, lo pongo en espera...
                    self.state.wait() # block until notified #bloqeuar hasta que se notifica
            try:
                if len(self.__eventos_tapa) != 0:#Hay un evento de puerta que guardar...
                    cur = self.__bd.cursor()
                    datos = self.__eventos_tapa[0:1] #Tomo los valores en el órden que se ingresados...                
                    cur.execute("""INSERT INTO EVENTOS_PUERTA (Fecha, Estado) VALUES (%s, %s)""",(datos[0][0], datos[0][1]))
                    self.__bd.commit()
                    print "Registro de evento tapa guardado.."
                    del self.__eventos_tapa[0:1] #Lo elimino de la tupla...
                if len(self.__eventos_motor_compuerta) != 0: #Han eventos de compuerta que guardar...
                    cur = self.__bd.cursor()
                    datos = self.__eventos_motor_compuerta[0:1] #Tomo los valores en el órden que se ingresados...
                    cur.execute("""INSERT INTO EVENTOS_COMPUERTA (Id_Pin_Compuerta, Fecha, Estado) VALUES (%s, %s, %s)""",(datos[0][0], datos[0][1], datos[0][2]))
                    self.__bd.commit()
                    print "Registro de evento compuerta guardado.."
                    del self.__eventos_motor_compuerta[0:1] #Lo elimino de la cola de mensajes...
                if len(self.__eventos_conexiones_red) !=0: #Hayeventos que registrar...
                    cur = self.__bd.cursor()
                    datos = self.__eventos_conexiones_red[0:1] #Tomo los valores en el órden que se ingresados...
                    cur.execute("""INSERT INTO EVENTOS_CONEXIONES_RED (Ip_Cliente, Puerto_Cliente, Estado, Fecha) VALUES (%s, %s, %s, %s)""",(datos[0][0], datos[0][1], datos[0][2], datos[0][3]))
                    self.__bd.commit()
                    print "Registro de evento conexión de red guardado.."
                    del self.__eventos_conexiones_red[0:1] #Una vez registrado, lo elimino de la cola de eventos....
                if len(self.__eventos_sensor_alimento) != 0: #Verifico si hay eventos que registrar...
                    cur = self.__bd.cursor()   
                    datos = self.__eventos_sensor_alimento[0:1] #Tomo los valores en el órden que se ingresados...
                    cur.execute("""INSERT INTO EVENTOS_SENSOR_ALIMENTO (Distancia, Fecha) VALUES (%s, %s)""",(datos[0][0], datos[0][1]))
                    self.__bd.commit()
                    print "Registro de evento sensor distancia guardado.."
                    del self.__eventos_sensor_alimento[0:1] #Una ves registrado en la BD, lo elimino...
                if len(self.__eventos_tarea_programada[0:1]) != 0: #Verifico si hay tareas por actualizar...
                    cur = self.__bd.cursor()   
                    datos = self.__eventos_tarea_programada[0:1] #Tomo los valores en el órden que se ingresados...
                    #cur.execute("Update CONFIG_APERTURAS_COMPUERTAS set Ejecutado=1 Where idCONFIG_APERTURAS_COMPUERTAS=" + datos[0])
                    cur.execute("""Insert into TAREAS_EJECUTADAS (Id_Config_Tarea, Fecha, Hora_Ejecucion) values(%s, %s, %s)""", (datos[0][0], datos[0][1],datos[0][2]))
                    self.__bd.commit()
                    print "Registro de evento tarea compuerta guardado..."
                    del self.__eventos_tarea_programada[0:1] #Una ves registrado en la BD, lo elimino...            
            except Exception as error:
                if 'MySQL server has gone away' in str(error): #Error OperationalError: (2006, 'MySQL server has gone away')
                    self.reconectar()
                else:
                    #self.__bd.rollback()
                    print error
                    print "Registro de evento CANCELADO..Error: " + error.message
            #self.pause()
            try:
                if self.__contador == self.__tiempo_actualizacion:
                    #print "Actualizando..."
                    cur = self.__bd.cursor()
                    cur.execute("show tables;")
                    self.__contador = 0
                self.__contador += 1
            except Exception as err:
                print err
            #print "Contador:", self.__contador    
            time.sleep(0.5)



    def resume(self):
        #Pone en marcha la ejecución del thread
        with self.state:
            self.paused = False
            self.state.notify()  # unblock self if waiting #desbloquear auto esperando

    def pause(self):
        #Para la ejecución del thread
        with self.state:
            self.paused = True  # make self block and wait #El hilo deja de ejecutarse, entra en estado de espera..
                    
    def reconectar(self):
        print "Intentando reconectar al servidor de BD..."
        try:
            #La conexión anterior ha caducado, tengo que reconectar...Se podría usar un pool de conexiones para resolver el problema...
            self.__bd = MySQLdb.connect(host=self.__serverBD, user=self.__usuario, passwd=self.__password, db=self.__nombreBD)
            self.__bd.ping(True) #reconexion automatica si es que se pierde....
        except Exception as error:
            print "Imposible conectar al servidor de datos.", error 
            
            
    def evento_tapa(self, hora, valor):
        #Evento que se deberá llamar cuando se cierra o se abre la tapa de la tolva...
        self.__eventos_tapa.append([hora, valor])
        
    
    def evento_motor_compuerta(self, id_pin_compuerta, fecha, valor_motor):
        #Evento que se deberá llamar cuando se acciona el motor de la compuerta...
        self.__eventos_motor_compuerta.append([id_pin_compuerta, fecha, valor_motor])

        
    def evento_conexion_red(self, ip_cliente, puerto_cliente, estado, fecha):
        #Evento que se deberá llamarse cuando se recibe una conexión o desconexión de un cliente...
        self.__eventos_conexiones_red.append([ip_cliente, puerto_cliente, estado, fecha])
        
    def evento_sensor_alimento(self, distancia, fecha):
        #Evento que se llama cuando se ejecuta una medición del sensor...
        self.__eventos_sensor_alimento.append([distancia, fecha])
        
    
    def evento_tarea_programada(self, tarea, fecha, hora):
        self.__eventos_tarea_programada.append([tarea, fecha, hora]) #Agrego los id de tareas a actualizar
    
        
    def detener_y_borrar(self):
        self.__bd.close() #Desconecto del servidor...
        
    def getBd(self):
        #hay que devolver una conexión viva y abierta...
        try:
            print "Devolviendo la BD..."
            cur = self.__bd.cursor()
            cur.execute("Select now()")
            if self.__bd.open == False: #Si la conexión no está abierta, vuelvo a reconectar con el servidor...
                self.reconectar()
            if (self.__bd):
                return self.__bd
            else:
                self.reconectar()
            return self.__bd
        except Exception as error:
            print "Error en getBd", error
            self.reconectar()
            return self.__bd

        
    def serverBD(self):
        return self.__serverBD
    
            
    def nombreBD(self):
        return self.__nombreBD
    
    
    def usuarioBD(self):
        return self.__usuario
    
    
    def passwordBD(self):
        return self.__password
        
    