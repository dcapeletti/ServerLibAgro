# coding=utf-8
'''
Copyright 2015 Diego Ariel Capeletti

Este archivo es parte del proyecto (((Dosificador alimenticio libre | Free food dispenser))).

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''
'''
Created on 06/12/2015

@author: diegoariel
'''

import threading
import time
from service_db import ServiceBD
from xml_database import ConfiguracionXML
#from server import MSGQueue
from servomotor import Servo, MotorSinThreads, MotorConThreads
from controlbutton import Boton
from rele import Rele
from sensorproximidad import SensorProximidad
from composite import Compuerta
from tareasprogramadas import GestorTareaProgramada
from serviceDateTime import ServiceDateTime

class Maquina():
    '''Clase que contiene métodos que se deben implementar en la herencia...
    Funciona como un patrón Strategy o estrategia.
    '''
    def inicializarMaquina(self):
        raise NotImplementedError('Debe implementar este método en la herencia..')
    
    def abrirServos(self):
        #Se llama por una tarea programada
        raise NotImplementedError('Debe implementar este método en la herencia..')
    
    def abrirServo(self, pin_servo, posicion):
        #Se llama por una tarea programada...
        raise NotImplementedError('Debe implementar este método en la herencia..')
    
    def cerrarServos(self):
        raise NotImplementedError('Debe implementar este método en la herencia..')
    
    def cerrarServo(self, pin_servo):
        raise NotImplementedError('Debe implementar este método en la herencia..')
    
    def sensarAlimento(self):
        raise NotImplementedError('Debe implementar este método en la herencia..')
    
    def actualizarTareaProgramada(self, id_compuerta=None):
        raise NotImplementedError('Debe implementar este método en la herencia..')
    
    def actualizarFechaHora(self, fecha_hora=None):
        raise NotImplementedError('Debe implementar este método en la herencia..')
    
    def detener(self):
        raise NotImplementedError('Debe implementar este método en la herencia..')
    

    
    
class MaquinaNormal(Maquina, threading.Thread):
    '''Estategia de inicializacion de la máquina en estado normal: puertas cerradas.'''
    def __init__(self, observador):
        print "Iniciando la máquina..."
        threading.Thread.__init__(self)
        self.daemon=True
        self.paused=True
        self.state=threading.Condition()
        #-----------------SENSORES Y ACTUADORES---------------
        self.__sensores_tapa_tolva={}
        self.__sensores_dictancia={}
        self.__compuertas={}
        #----------------------------------------------------
        self.__frecuencia_ejecucion_thread=1 #Valor por defecto 1 seg...
        self.__loop_thread=True #Ejecucion del hilo de manera permanente...
        
        self.__gestoresTareasProgramadas=[] #Lista de gestores de tareas de las compuertas
        self.__cola_mensajes = observador #MSGQueue() #Observador de eventos
        self.__cola_mensajes.setMaquina(self)
        self.__config_xml = ConfiguracionXML("./Config.xml") #Objeto que manipula la configuración xml...
        self.__service_bd = ServiceBD(serverBD=self.__config_xml.getValorNombreServidor(), nombreBD=self.__config_xml.getValorNombreBD(), usuario="dcapeletti", password="33426000")
        self.__service_bd.start()
        
        self.__service_datetime = ServiceDateTime() #Prueba
        self.__service_datetime.start()
        self.actualizarFechaHora()
        #self.__service_datetime.actualizarFechaYHoraDesdeInternet()
        
        self.inicializarMaquina()
    
    def run(self):
        #self.resume() Al hacer start() no lo inicio al thread, solo cuando se hace resume
        while True:
            with self.state:
                if self.paused: #Se evalua el estado del hilo, si esta pausado, lo pongo en espera...
                    self.state.wait() # block until notified #bloqeuar hasta que se notifica
            
            
            #Leer por cada x tiempo, los registros de bd y actualizar el modelo...¿Lo debe hacer un thread o un mensaje del cliente?
            if self.__loop_thread == False:
                self.pause() #Si el loop es Falso, paro la ejecución del thread hasta que se llame a resume()            
            
            time.sleep(self.__frecuencia_ejecucion_thread)
        
    
    def resume(self):
        #Pone en marcha la ejecución del thread
        with self.state:
            self.paused = False
            self.state.notify()  # unblock self if waiting #desbloquear auto esperando

        
    def pause(self):
        #Para la ejecución del thread
        with self.state:
            self.paused = True  #El hilo deja de ejecutarse, entra en estado de espera..

    
    def inicializarMaquina(self):
        self.crearCompuertas()
        self.crearBotonesTapaSuperior()
        self.crearSensoresProximidad()
        self.cerrarServos() #Al iniciar la máquina se cierran los motores.
        
    def crearCompuertas(self):
        #Creación e inicializacion de las compuertas...
        print "Inicializando las compuertas..."
        cur = self.__service_bd.getBd().cursor()
        try:
            cur.execute("SELECT * FROM COMPUERTAS") #Tengo TODAS LAS COMPUERTAS y sus configuraciones...
            filas = cur.fetchall()
                
            for fila in filas: #Para todas las filas, creo un objeto compuerta...
                valor = int(fila[1])  #Columna Id_compuerta
                print "    Creando compuerta ", valor
                comp = Compuerta(valor)
                comp.add(self.crearServo(int(fila[2]))) #Creo el servo pasando el valor del pin...
                boton = self.crearBotonCompuerta(int(fila[3])) #Creo el objeto boton de la compuerta...
                boton.setMensajeNotificacion("botonCompuerta comp=" + str(valor))
                comp.add(boton) #agrego el boton
                if int(fila[4]) !=0: #Solo si es distinto de cero creo el rele...
                    comp.add(self.crearRele(int(fila[4]))) #Creo el rele...
                gestor = GestorTareaProgramada(comp, self) #Gestor de tareas para la compuerta...
                gestor.start()
                gestor.add_observer(self.__cola_mensajes)
                self.__gestoresTareasProgramadas.append(gestor)
                self.__compuertas[valor] = comp                
        except Exception as error:
            print "Error al inicialzar las compuertas...", error
            
    
    def crearServo(self, pin_servo):
        #servo = Servo(pin_servo)
        #servo = MotorSinThreads(pin_servo)
        servo = MotorConThreads(pin_servo)
        servo.start()
        servo.add_observer(self.__cola_mensajes)
        return servo
    
    def crearBotonCompuerta(self, pin_boton):
        btn = Boton(pin_boton, "botonCompuerta") #El mensaje aqui es inutil
        btn.setFrecuencia_actualizacion_thread(0.1)
        btn.start()
        btn.add_observer(self.__cola_mensajes)
        return btn
    
    def crearRele(self, pin_rele):
        rele = Rele(pin_rele, "")
        rele.add_observer(self.__cola_mensajes)
        rele.start()
        return rele
    
    def crearBotonesTapaSuperior(self):
        #Crea los objetos sensores de tapa de tolva...
        print "Creando objetos sensores de tapa de tolva..."
        cur = self.__service_bd.getBd().cursor()
        try:
            cur.execute("SELECT OBJETOS_VALORES_PROPIEDADES.Id_grupo_Sensor, OBJETOS.Nombre_objeto, PROPIEDADES_OBJETOS.Nombre_propiedad, " +
                        " OBJETOS_VALORES_PROPIEDADES.Valor from OBJETOS_VALORES_PROPIEDADES inner join " +
                        " OBJETOS on OBJETOS_VALORES_PROPIEDADES.Objeto = OBJETOS.idOBJETOS inner join " +
                        " PROPIEDADES_OBJETOS on PROPIEDADES_OBJETOS.idPROPIEDADES_OBJETOS = " + 
                        " OBJETOS_VALORES_PROPIEDADES.Propiedad Where OBJETOS_VALORES_PROPIEDADES.Activa=1 " +
                        " and OBJETOS.Nombre_objeto='SensorTapa'")
            rows = cur.fetchall()
            
            for row in rows :
                valor = int(row[3])
                sensor_tapa = Boton(valor, "sensorTapa") #Creo el objeto boton para controlar las compuertas con la configuración del pin y el mensaje que dispara ante el evento...
                sensor_tapa.setEstadoInicialBoton(0) #Por defecto la tapa superior deberá estar cerrada...
                self.__sensores_tapa_tolva[valor] = sensor_tapa #Agrego el objeto con la clave del pin...
                sensor_tapa.add_observer(self.__cola_mensajes) #Lo agrego al observador
                sensor_tapa.start() #Inicio el thread
                print "Estado actual boton tapa superior ", sensor_tapa.get_estado_boton()
        except Exception as error:
            print "Error en la creación del sensor de tapa de tolva...", error
    
    
    def crearSensoresProximidad(self):
        #Crea los objetos rele para las compuertas y motoes vibradores...
        print "Creando objetos sensores de proximidad..."
        cur = self.__service_bd.getBd().cursor()
        try:
            cur.execute("SELECT Id_grupo_Sensor FROM OBJETOS_VALORES_PROPIEDADES " + 
                    " where Objeto=2 and Activa=1"+
                    " group by Id_grupo_Sensor")
            grupos_sensores = cur.fetchall() #Tengo el grupo de sensores...
            #print grupos_sensores[0][0]
            for i in grupos_sensores:
                #print "Grupo", i[0]
                cur.execute("SELECT OBJETOS_VALORES_PROPIEDADES.Id_grupo_Sensor, OBJETOS.Nombre_objeto, PROPIEDADES_OBJETOS.Nombre_propiedad, " +
                        " OBJETOS_VALORES_PROPIEDADES.Valor from OBJETOS_VALORES_PROPIEDADES inner join " +
                        " OBJETOS on OBJETOS_VALORES_PROPIEDADES.Objeto = OBJETOS.idOBJETOS inner join " +
                        " PROPIEDADES_OBJETOS on PROPIEDADES_OBJETOS.idPROPIEDADES_OBJETOS = " + 
                        " OBJETOS_VALORES_PROPIEDADES.Propiedad Where OBJETOS_VALORES_PROPIEDADES.Activa=1 " +
                        " and OBJETOS.Nombre_objeto='SensorProx' " +
                        " and OBJETOS_VALORES_PROPIEDADES.Id_grupo_Sensor=" + str(i[0]))
                valores = cur.fetchall()
                sensor_prox = SensorProximidad(0,0, "sensorProx") #Inicializo los pines en cero
                for prop in valores:
                    valor_pin = int(prop[3]) #Valor de pin conectado al sensor...
                    if prop[2] == "pin_echo":
                        #print prop[2], valor_pin
                        sensor_prox.setPin_echo(valor_pin)
                    elif prop[2] == "pin_trig":
                        #print prop[2], valor_pin
                        sensor_prox.setPin_trig(valor_pin)
                sensor_prox.inicializar_sensor() #Inicio el sensor con la configuracion de los pines...
                sensor_prox.add_observer(self.__cola_mensajes)
                sensor_prox.start()
                self.__sensores_dictancia[i[0]] = sensor_prox
        
        except Exception as error:
            print "Error en la creación de los sensores de proximidad...", error

    
    def abrirServos(self, posicion=0):
        #Abrir todos los servos...
        for comp in self.__compuertas: #De todas las compuertas, obtengo el motor y lo abro...
            servo = self.__compuertas[comp].getMotor()
            if posicion ==0:
                servo.abrir()
            else:
                print "Abriendo los servos"
                servo.set_posicion_servo(int(posicion))
            
    def cerrarServos(self):
        #Cerrar todos los servos...
        for comp in self.__compuertas: #De todas las compuertas, obtengo el motor y lo cierro...
            servo = self.__compuertas[comp].getMotor()
            servo.cerrar()
            
    def abrirServo(self, pin_servo, posicion=0):
        #Abro el servo en la posición indicada...
        for comp in self.__compuertas:
            if self.__compuertas[comp].getMotor().get_pin_servo_motor() == pin_servo:
                servo = self.__compuertas[comp].getMotor()
                if posicion == 0:
                    servo.abrir()
                else:
                    servo.set_posicion_servo(int(posicion))
        
    def cerrarServo(self, pin_servo):
        #Busco el servo en todas las compuertas y lo cierro...
        for comp in self.__compuertas:
            if self.__compuertas[comp].getMotor().get_pin_servo_motor() == pin_servo:
                servo = self.__compuertas[comp].getMotor()
                servo.cerrar()
                break #Una vez que lo cierro, salgo del bucle...
        
    def sensarAlimento(self):
        #Realizar monitoreo del sensor de alimento...
        for valor in self.__sensores_dictancia:
            self.__sensores_dictancia[valor].resume()
            
    
    def actualizarTareaProgramada(self, id_compuerta=None): 
        '''Cuando un cliente hace un cambio en las tareas de una o varias compuertas,
        se recibe una tupla con los valores de identificadores de las compuertas en 
        forma de cadena separada por comas 1, 15, 8, etc. Si se quiere actualizar 
        las tareas de todas las compuertas, se envía el mensaje "Todas".
        '''
        #Este método permite la actualización de las tareas programadas para las compuertas
        if id_compuerta == None: #Si no recibo ningún id de compuerta, actualizo las tareas de TODAS las compuertas...
            for obj in self.__gestoresTareasProgramadas:
                #obj.cargarTareas()
                obj.cargarTareasDelDia()
        else: #Cargo las tareas de una compuerta dada
            for valor in id_compuerta:
                for gestor_tarea_compuerta in self.__gestoresTareasProgramadas:
                    if gestor_tarea_compuerta.getCompuerta().getIdCompuerta() == int(valor):
                        #gestor_tarea_compuerta.cargarTareas()
                        gestor_tarea_compuerta.cargarTareasDelDia()
                        
    
                        
    def actualizarFechaHora(self, fecha_hora=None):
        '''Este método ordena la instalación de fecha y hora en el servidor.
        El usuario puede instalar la fecha y hora por medio del parametro.
        Si el parametro esta vacio se busca una confg en la BD.'''
        serverNTP = None
        Actualizar_Automaticamente = False
        res = None
        if fecha_hora == None: #Si el parametro esta vacío busco una config en la BD
            cur = self.__service_bd.getBd().cursor()
            cur.execute("Select ServidorNTP, Actualizar_Automaticamente from CONFIGURACION_DATETIME Where Activa=1")
            #print cur.rowcount
            res = cur.fetchall()
        
        if res != None:    
            for val in res:
                if val[1] == 1: #Actualizar automaticamente
                    serverNTP = val[0]
                    Actualizar_Automaticamente = val[1]
                    #self.__service_datetime.actualizarFechaYHoraDesdeInternet(serverNTP)
                
        if fecha_hora == None and Actualizar_Automaticamente == True and serverNTP != None:
            self.__service_datetime.actualizarFechaYHoraDesdeInternet(serverNTP)
        elif fecha_hora != None:
            self.__service_datetime.configurarFechaYHora(float(fecha_hora))
    
        
    def detener(self):
        #Cuando se apaga la máquina, se detiene la configuración de los componentes...
        print "Deteniendo la máquina..."
        self.cerrarServos() #Cierro todos los servos...            
        for pin_sensor in self.__sensores_tapa_tolva:
            self.__sensores_tapa_tolva[pin_sensor].detener_y_borrar()
            
        for pin_sensor_dist in self.__sensores_dictancia:
            self.__sensores_dictancia[pin_sensor_dist].detener_y_borrar()
            
        for comp in self.__compuertas:
            #print self.__compuertas[comp].getComponentes()
            self.__compuertas[comp].getMotor().detener_y_borrar()
            self.__compuertas[comp].getRele().detener_y_borrar()
            self.__compuertas[comp].getBotonCompuerta().detener_y_borrar()
            
        self.__service_bd.detener_y_borrar()
        
        
    def setFrecuenciaActualizacionThread(self, valor=1):
        #Por defecto es 1 segundo...
        self.__frecuencia_ejecucion_thread=valor
        
    def setLoopThread(self, valor=True):
        #Establece si el hilo se debe ejecutar de manera permanente o no...
        self.__loop_thread=valor
        
    def getServiceBd(self):
        return self.__service_bd
    
    def getServiceXML(self):
        return self.__config_xml
    
        
        
        
        
