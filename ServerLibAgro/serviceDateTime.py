# coding=utf-8
'''
Copyright 2015 Diego Ariel Capeletti

Este archivo es parte del Dosificador T4C del proyecto LibAgro. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''

'''
Created on 09/10/2015

@author: diegoariel
Este archivo proporciona el servicio de actualización y configuración de fecha
y hora para el servidor.
Hay 3 posibles maneras de actualizar:
1) Actualizar desde un servidor NTP. Es posible en el cliente establecer para
que el servidor actualice automáticamente al arrancar. En caso de que la actualización
falle por conexión a la red, el thread queda esperando un tiempo y realiza varios 
reintentos esperando cierto tiempo entre uno y otro. Una vez que finaliza, el thread
queda en pausa.

2) Actualizar desde el cliente. El programa cliente obtiene los datos de fecha y hora
desde el ordenador que se está ejecutando. Un usuario autorizado, puede instalar
esos de fecha y hora en el servidor.

3) Actualización manual. Un usuario autorizado ingresa una fecha y hora desde el 
programa cliente y envía esos datos al servidor.

PREGUNTAS A RESOLVER...
El equipo se queda sin energía y vuelve arrancar pero no puede en ningun momento
descargar el horario de la red. ¿Debería dejar un registro de tal error para que
un cliente al conectarse pueda estar enterado de la situación?

El equipo despues de bastante tiempo instala el horario de red o un usuario
instala una nueva fecha y hora, ¿los horarios de tareas programadas cargadas 
previamente en memoria se ejecutan correctamente?.
Si, las tareas automáticas son eliminadas y recargadas inmediatamente al cambiar 
de fecha. 
'''
import time
import threading
from observable import Observable 
import ntplib
import os

class ServiceDateTime(threading.Thread, Observable):
    def __init__(self, servidorNTP=None):
        threading.Thread.__init__(self)
        Observable.__init__(self)
        #self.mensaje_notificacion = mensaje #Mensaje que se envía al mediador ante eventos del sensor.
        self.servidorNTP = servidorNTP #pool.ntp.org
        self.actualizacionCorrecta = False
        self.daemon=True
        self.paused=True #Inicio pausado
        self.state=threading.Condition()
        
        
    def run(self):
        #self.resume() Al hacer start() no lo inicio al thread, solo cuando se hace resume
        while True:
            with self.state:
                if self.paused: #Se evalua el estado del hilo, si esta pausado, lo pongo en espera...
                    self.state.wait() # block until notified #bloqeuar hasta que se notifica
            #¿Envío la fecha y hora del servidor al cliente cuando se conecta?
            if self.actualizacionCorrecta == False:
                #self.actualizarFechaYHoraDesdeInternet()
                #time.sleep(5000) #esperar 5 minutos y volver a intentar
                try:
                    response = self.__client.request(self.servidorNTP) #Obtengo de la BD, el servidor enviado por el cliente
                    #print (time.localtime(response.tx_time))
                    #print time.strftime('%Y/%m/%d %H:%M:%S',time.localtime(response.tx_time))
                    print "Instalando la fecha:", time.strftime('%d/%m/%Y %H:%M:%S',time.localtime(response.tx_time))
                    os.system('date ' + time.strftime('%m%d%H%M%Y.%S',time.localtime(response.tx_time)))
                    self.actualizacionCorrecta = True
                    self.pause()
                except: #Si existe un error, el thread debe continuar probando hasta que funciona la red
                    print "No se ha podido conectar al servidor NTP. Reintentando..."
                    self.actualizacionCorrecta = False
                    time.sleep(10) #esperar 10 segundos y volver a intentar...
                    #Guardar en el log que no pudo actualizar desde que arranco...
                    #self.resume()
            
            #print "Distancia: ", distance, " cm"
            #mensaje = "<"+ self.mensaje_notificacion +">" + str("Horario correctamente instalado") + "</"+ self.mensaje_notificacion +">\n" #La distancia SIEMPRE es en cm...
            #self.notify_observers(mensaje)
            #self.pause() #Pongo en pausa
            
       
        
    def resume(self):
        #Pone en marcha la ejecución del thread
        with self.state:
            self.paused = False
            self.state.notify()  # unblock self if waiting #desbloquear auto esperando

    def pause(self):
        #Para la ejecución del thread
        with self.state:
            self.paused = True  #El hilo deja de ejecutarse, entra en estado de espera..
            
    
    def actualizarFechaYHoraDesdeInternet(self, servidorNTP):
        #Este procedimiento solo se ejecuta al arrancar la máquina o por orden del cliente.
        self.actualizacionCorrecta = False
        self.servidorNTP = servidorNTP
        self.__client = ntplib.NTPClient()
        self.resume()  
        
    
    def configurarFechaYHora(self, fecha_y_hora):
        print "Instalando la fecha y hora: ", time.strftime('%d/%m/%Y %H:%M:%S',time.localtime(fecha_y_hora))
        os.system('date ' + time.strftime('%m%d%H%M%Y.%S',time.localtime(fecha_y_hora)))
        
    def getFechaYHora(self):
        #Si el cliente pide la hora del servidor, se la devuelve 
        return time.strftime('%d/%m/%Y %H:%M:%S')
    
    
    def detener_y_borrar(self):
        self.pause()
        
    
        
        
  