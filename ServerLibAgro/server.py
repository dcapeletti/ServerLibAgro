#!/bin/env python
# coding=utf-8
'''
Copyright 2009 Tim Bower, 2015 Diego Ariel Capeletti: Apache License, Version 2.0
**Archivo:** server.py 
This program was developed for education purposes for the Network
Programming Class, CMST 355, at Kansas State University at Salina.

Este archivo es parte del proyecto (((Dosificador alimenticio libre | Free food dispenser))).
Este programa es un servidor multi threads pensado para el manejo de múltiples sensores y actuadores 
por medio de comunicación de red TCP/IP. Esta pensado para adaptarlo a propósitos generales
a través de la creación de un protocolo simple para control de GPIO de ordenadores single board.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''

'''
Para desarrollar en eclipse deberá instalar los paquetes python-dev:

sudo apt-get install python-dev

Luego instalar el módulo para controlar las librerías GPIO de raspberry:

sudo pip install RPi.GPIO
'''

import socket
import sys
import os
import threading
import time, datetime
from itertools import cycle 
from Observer import Observer
from strategy import MaquinaNormal

MAX_INDEX = 100                      # El máximo del índice cíclico
MAX_LEN = 10                         # Longitud de la cola de mensajes
host = ''                            # Enlazar a todas las interfaces de red
port = 50000

def mesg_index(old, last, new):
    """
    :param old: indice entero del mensaje más antiguo (primero) en la cola
    :param last: índice entero del último mensaje leído por el subproceso de cliente
    :param new: índice entero del mensaje más reciente (último) en la cola

    This computes the index value of the message queue from where the reader
    should return messages.  It accounts for having a cyclic counter.
    This code is a little tricky because it has to catch all possible
    combinations.
    
    
    Este calcula el valor del índice de la cola de mensajes desde donde el lector
    debe devolver mensajes. Es responsable de tener un contador cíclico.
    Este código es un poco difícil porque tiene que coger todos los posibles
    combinaciones.
    """
    if new >= old:
        # normal case #Caso normal
        if last >= old and last < new:
            return (last - old + 1)
        else:
            return 0
    else:
        # cyclic roll over (new < old)
        if last >= old:
            return (last - old + 1)
        elif last < new:
            return (MAX_INDEX - old + last)
        else:
            return 0
        

class MSGQueue(object, Observer):
    """
    Manage a queue of messages for Chat, the threads will read and write to
    this object.  This is an implementation of the readers - writers problem
    with a bounded buffer.
    
    
    Clase para administrar una cola de mensajes, los threads (clientes, sensores y actuadores) 
    van a leer y escribir a este objeto (Observador). Esta clase es una implementación de algoritmos 
    lectores - escritores con un búfer limitado.
    """
    def __init__(self):
        self.msg = [] #Tupla o Cola de mensajes
        self.cyclic_count = cycle(range(MAX_INDEX)) #Obtengo el objeto cíclico
        self.current = -1
        self.readers = 0
        self.writers = 0
        self.mutex1 = threading.Lock()
        self.mutex2 = threading.Lock()
        self.readPending = threading.Lock()
        self.writeBlock = threading.Lock()
        self.readBlock = threading.Lock()
        self.maquina=0

# This is kind of complicated locking stuff. Don't worry about why there
# are so many locks, it just came from a book showing the known solution to
# the readers and writers problem. You may wonder if so many locks and
# semaphores are really needed.  Well, lots of of really smart people have
# studied the readers and writers problem and this is what they came up
# with as a solution that works with no deadlocks.  The safe plan for us is
# to just use a known solution.

# The only code I did not take directly from a book is what's inside the
# critical section for the reader and writer. The messages are kept in a
# list.  Each list is a tuple containing an index number, a time stamp and
# the message.  Each thread calls the reader on a regular basis to check if
# there are new messages that it has not yet sent to it's client.
# To keep the list from growing without bound, if it is at the MAX_LEN size,
# the oldest item is removed when a new item is added.

# The basic idea of the readers and writers algorithm is to use locks to
# quickly see how many other readers and writers there are.  writeBlock is the
# only lock held while reading the data.  Thus the reader only prevents writers
# from entering the critical section.  Both writeBlock and readBlock are held
# while writing to the data.  Thus the writer blocks readers and other writers.
# So, multiple readers or only one writer are allowed to access the data at a
# given time.  Multiple readers are allowed because the reader does not modify
# the data.  But since the writer does change the data, we can only allow one
# writer access to the critical section at a time.  We also don't want a reader
# in the critical section while a writer is there because the writer could mess
# up the reader by changing the data in the middle of it being read.


    '''
    Esto es un poco complicado cosas bloqueo. No te preocupes por eso que hay
    tantas cerraduras, que acaba de llegar de un libro que muestra la solución conocida 
    para los lectores y escritores problema. Usted puede preguntarse si tantas cerraduras
    y semáforos son realmente necesarios. Bueno, mucha gente realmente inteligentes han
    estudiado el lectores y escritores problema y esto es lo que les ocurrió como una
    solución que funciona sin puntos muertos. El plan mas seguro para nosotros es que 
    sólo tiene que utilizar una solución conocida.

    El único código que no tomé directamente de un libro es lo que hay dentro de la
    sección crítica para el lector y el escritor. Los mensajes se mantienen en una
    lista. Cada lista es una tupla que contiene un número de índice, un sello de tiempo y
    el mensaje. Cada subproceso llama al lector sobre una base regular para comprobar si 
    hay nuevos mensajes que aún no ha enviado a el cliente.
    Para mantener la lista de crecer sin límite, si es en el tamaño MAX_LEN, el elemento
    más antiguo se elimina cuando se añade un nuevo elemento.

    La idea básica de los algoritmos lectores y escritores es usar cerraduras para
    ver rápidamente cómo muchos otros lectores y escritores existen. WriteBlock es el
    único bloqueo mantenido durante la lectura de los datos. Así, el lector sólo 
    evita que escritores de entrar en la sección crítica. Tanto WriteBlock y ReadBlock 
    se llevan a cabo durante la escritura de los datos. Así, los lectores bloques escritor y otros escritores.
    Así, varios lectores o un único escritor se les permite acceder a los datos en un
    momento dado. Múltiples lectores se permiten porque el lector no modifica los datos.
    Pero desde que el escritor cambia los datos, sólo nos podemos permitir un acceso del 
    escritor a la sección crítica a la vez. Tampoco queremos un lector en la sección 
    crítica mientras un escritor está ahí porque el escritor podría estropear el lector 
    al cambiar los datos en el medio de ella está leyendo.
    '''



    def reader(self, lastread):
        "Reader of readers and writers algorithm"
        "Lector de lectores y escritores algoritmo"
        '''Función que devuelve una tupla con el mensaje'''
        self.readPending.acquire()
        self.readBlock.acquire()
        self.mutex1.acquire()
        self.readers = self.readers + 1
        if self.readers == 1: 
            self.writeBlock.acquire()
        self.mutex1.release()
        self.readBlock.release()
        self.readPending.release()
        # here is the critical section
        if lastread == self.current: # or not len(self.msg):
            retVal = None
        else:
            MsgIndex = mesg_index(self.msg[0][0], lastread, self.current) #Retorna el número del mensaje
            retVal = self.msg[MsgIndex:]
        # End of critical section
        self.mutex1.acquire()
        self.readers = self.readers - 1
        if self.readers == 0: self.writeBlock.release()
        self.mutex1.release()
        return retVal

    def writer(self, data):
        "Writer of readers and writers algorithm"
        "Escritor de lectores y escritores algoritmo"
        self.mutex2.acquire()
        self.writers = self.writers + 1
        if self.writers == 1: 
            self.readBlock.acquire()
        self.mutex2.release()
        self.writeBlock.acquire()
        # here is the critical section
        self.current = self.cyclic_count.next() #Incremento
        self.msg.append((self.current, time.localtime(), data))
        while len(self.msg) > MAX_LEN: #Evalua el tamaño de la cola de mensajes
            del self.msg[0]     # remove oldest item #Elimino los mensajes viejos
        # End of critical section
        self.writeBlock.release()
        self.mutex2.acquire()
        self.writers = self.writers - 1
        if self.writers == 0: self.readBlock.release()
        self.mutex2.release()
        
        
    def update(self, observable, event):
        #Escribo el evento en la cola de mensajes...
                
        if "<botonCompuerta comp=" in event:
            valor_boton=int(event[event.find(">")+1:event.find("</")])
            if valor_boton == 0:
                #self.maquina.abrirServos() #Abre TODOS los servos...
                observable.getParent().getMotor().abrir() #Obtengo el contendor padre (Compuerta) del objeto Boton...
            else:
                #self.maquina.cerrarServos() #Cierra TODOS los servos...
                observable.getParent().getMotor().cerrar() #Obtengo el contendor padre (Compuerta) del objeto Boton...
        if "<sensorTapa>" in event: #Evento de tapa superior...
            valor = event[event.find("<sensorTapa>")+12:event.find("</sensorTapa>")]
            print "Evento de tapa...", valor
            #maquina.getServiceBd().evento_tapa(time.strftime('%Y-%m-%d %H:%M:%S'), valor)
            self.maquina.getServiceBd().evento_tapa(time.strftime('%Y-%m-%d %H:%M:%S'), valor)
        if "<estadoServo>" in event: #Evento del apertura del motor. Este mensaje solo se guarda en la bd, no se envía al cliente...
            valor = event[event.find("<estadoServo>")+13:event.find("</estadoServo>")]
            print "Evento de compuerta...", valor
            self.maquina.getServiceBd().evento_motor_compuerta(observable.getParent().getIdCompuerta(), str(datetime.datetime.now()), valor)
            return
        if "<sensorProx>" in event: #Evento del sensor distancia...
            valor = event[event.find("<sensorProx>")+12:event.find("</sensorProx>")]
            self.maquina.getServiceBd().evento_sensor_alimento(valor, time.strftime('%Y-%m-%d %H:%M:%S'))
        if "<tareaprogramadaejecutada>" in event:
            #tarea = event[event.find("<tareaprogramadaejecutada>")+26:event.find("</tareaprogramadaejecutada>")]
            #print "Guardando tarea " + tarea
            tarea_ejecutada = observable.getTareaActual() #Tipo TareaAbrirServo
            print "Guardando tarea ", str(tarea_ejecutada.getidTarea())
            hora_ejecucion = str(tarea_ejecutada.getInicio().hour) + ":" + str(tarea_ejecutada.getInicio().minute) + ":" + str(tarea_ejecutada.getInicio().second)
            self.maquina.getServiceBd().evento_tarea_programada(tarea_ejecutada.getidTarea(), time.strftime('%Y-%m-%d'), hora_ejecucion)
            #print observable.__class__
            
        self.writer(event)
       
        
    def setMaquina(self, Maquina):
        self.maquina = Maquina


def sendAll(sock, lastread):
    "Get any unread messages and send them to the client"
    "Obtener los mensajes no leídos y los envío al cliente..."
    global chatQueue
    reading = chatQueue.reader(lastread) #Obtengo la tupla [Id|Hora¡mensaje]
    if reading == None: return lastread
    for (last, timeStmp, msg) in reading: #Guardo los valores de la lista en cada variable
        sock.send("El %s -- %s" % (time.asctime(timeStmp), msg))
    return last

def clientExit(sock, peer, error=None):
    "Report that a client has exited the chat session."
    "Informar que un cliente se ha desconectado..."
    # this function just cuts down on some code duplication
    # Esta función simplemente reduce cierta duplicación de código
    global chatQueue
    print "Una desconexión desde " + peer
    if error: #Verifico si ha salido con errores
        msg = peer + " has exited -- " + error + "\r\n"
    else:
        msg = peer + " has exited\r\n"
    chatQueue.writer(msg)

def handlechild(clientsock):
    """    
    Función que se postuló como el hilo para manejar la conexión de cliente.
    Lo hace el trabajo de envío y recepción de los datos de un cliente.
    """
    global chatQueue
    global maquina
    # lastreads of -1 gets all available messages on first read, even 
    # if message index cycled back to zero.
    # lastreads de -1 obtiene todos los mensajes disponibles en primera lectura, incluso
    # si índice de mensajes ciclo de nuevo a cero.
    lastread = -1
    # the identity of each user is called peer - they are the peer on the other
    # end of the socket connection. 
    # la identidad de cada usuario se llama peer - que son los pares en el otro
    # final de la conexión de socket.
    peer = clientsock.getpeername() #Retorna una tupla con la dirección remota del socket (dirección, puerto)
    print "Nueva conexión desde ", peer
    msg = str(peer) + " se ha unido\r\n"
    chatQueue.writer(msg)
    while 1:
        # check for and send any new messages
        # buscar y enviar mensajes nuevos
        lastread = sendAll(clientsock, lastread)
        try:
            data = clientsock.recv(4096)
        except socket.timeout:
            continue
        except socket.error:
            # caused by main thread doing a socket.close on this socket
            # It is a race condition if this exception is raised or not.
            # causada por hilo principal haciendo un Socket.close en este zócalo
            # Es una condición de carrera si esta excepción se eleva o no.
            print "Servidor apagado"
            return
        except:  # some error or connection reset by peer # algún error o conexión restablecer por pares
            clientExit(clientsock, str(peer))
            break
        if not len(data): # a disconnect (socket.close() by client) # una desconexión (Socket.close () por el cliente)
            clientExit(clientsock, str(peer))
            break

        # Process the message received from the client 
        # Procesar el mensaje recibido desde el cliente
        # First check if it is a one of the special chat protocol messages.
        # En primer lugar comprobar si es un uno de los mensajes especiales de protocolo de chat.
        if data.startswith('/nick'):  # Verifico el comienzo del mensaje
            oldpeer = peer
            peer = data.replace('/nick', '', 1).strip() #Reemplazo /nick por vacio
            if len(peer): #Si el largo es > 0
                chatQueue.writer("%s ahora es %s\r\n" \
                                % (str(oldpeer), str(peer)))
            else: peer = oldpeer

        elif data.startswith('/quit'): #Verifico si es una salida "Desconectar"
            bye = data.replace('/quit', '', 1).strip()
            if len(bye):
                msg = "%s se ha desconectado -- %s\r\n" % (str(peer), bye)
            else:
                msg = "%s se ha desconectado\r\n" % (str(peer))
            maquina.getServiceBd().evento_conexion_red(clientsock.getpeername()[0], clientsock.getpeername()[1], 0, time.strftime('%Y-%m-%d %H:%M:%S'))
            chatQueue.writer(msg)
            break            # exit the loop to disconnect # salgo del bucle para desconectar

        else:
            # Not a special command, but a chat message
            chatQueue.writer("Mensaje desde %s:\r\n\t%s\r\n" \
                                % (str(peer), data))
            
            if data == "aservo": #Abro el servo
                maquina.abrirServos()
            elif data == "cservo":
                maquina.cerrarServos()
            elif data == "sservo":
                #ser.ciclar()
                pass
            elif data.find("pservo") != -1:
                print "Abriendo los servos"
                posicion = data[data.find(":")+1: data.find(":")+3]
                maquina.abrirServos(posicion)
            elif data.find("<moverServo id=") != -1: #Cuando se mueve el slider de alguna compuerta de la GUI
                id_servo = data[data.find("=")+1:data.find(">")]
                posicion = data[data.find(">")+1:data.find("</")]
                maquina.abrirServo(int(id_servo), int(posicion))
                
            if data == "dist":
                maquina.sensarAlimento()
            
            if data =="arele": #Abreo el rele
                #rele.activar()
                pass
            elif data == "crele":
                #rele.desactivar()
                pass
            
            if data.find("tareaprogramada") != -1: #Se recibe el mensaje del protocolo para recargar las tareas programadas
                #El valor de data debe ser una cadena "1, 8, etc" donde cada valor es una compuerta. Todas, actualiza las tareas de todas las compuertas.
                comp =  data[data.find("<tareaprogramada>")+17:data.find("</tareaprogramada>")]
                if comp == "Todas": #Actualizo las tareas de todas las compuertas
                    comp = None
                else: #Se recibe una cadena con las compuertas
                    if len(comp) > 1:
                        comp = list(eval(comp))
                maquina.actualizarTareaProgramada(comp)
            
            #En este apartado se reciben los comandos de actualización de fecha y hora desde el cliente    
            if data == "actualizarHoraInternet":
                maquina.actualizarFechaHora()
            elif data.find("actualizar_fecha_hora") != -1: #Si se recibe un mensaje del cliente con dicha informacion, actualizar la fecha...
                nueva_fecha = data[data.find("<actualizar_fecha_hora>")+23:data.find("</actualizar_fecha_hora>")]
                maquina.actualizarFechaHora(nueva_fecha)
                                

    #-- End looping for messages from/to the client
    # Terminar bucle para los mensajes desde / hasta el cliente
    # Close the connection
    # Cierro la conexion
    clientsock.close()

# Begin the main part of the program
# Comienzo de la parte principal del programa
def main():
    """
    The parent thread that listens for connections and spawns a child thread
    to handle each connection.
    
    El hilo principal que escucha las conexiones y genera un hilo hijo para manejar 
    cada conexión.
    """
    global chatQueue
    global maquina
    clients = []

    # Set up the socket.
    # Configuración del Socket
    socket_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socket_server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    socket_server.bind((host, port))
    socket_server.listen(3)

    while 1:
        print "Esperando conexiones..."
        try:
            clientsock, clientaddr = socket_server.accept() #Acepto y tomo el socket y la dirección de destino.
            # set a timeout so it won't block forever on socket.recv().
            # Clients that are not doing anything check for new messages 
            # after each timeout.
            #clientsock.settimeout(1)
            clientsock.settimeout(0.1)
            time.sleep(0.1)
        except KeyboardInterrupt:
            # shutdown - force the threads to close by closing their socket
            # shutdown - forzar los hilos para cerrar su zócalo

            maquina.detener() 
            socket_server.close()
            for sock in clients:
                sock.close()
            break
        #except:
        #    traceback.print_exc()
        #    continue 
        maquina.getServiceXML().actualizarConfiguracion() #Releemos el archivo XML para actualizar la configuración...        
        maquina.getServiceBd().evento_conexion_red(clientsock.getpeername()[0], clientsock.getpeername()[1], 1, time.strftime('%Y-%m-%d %H:%M:%S'))
        clientsock.send(maquina.getServiceXML().getNombreServidorBD()+ "\n"+ maquina.getServiceXML().getNombreBD()) # Se envía al usuario que se conecta la información que se necesite...        
        clients.append(clientsock) #Agrego el socket a la lista de clientes.
        t = threading.Thread(target = handlechild, args = [clientsock])
        t.setDaemon(1)
        t.start()   

if __name__ == '__main__':
    # Una cola de mensajes global, que utiliza algoritmos de sincronización 
    # lectores y escritores
    
    chatQueue = MSGQueue()
    maquina = MaquinaNormal(chatQueue)
    main()
