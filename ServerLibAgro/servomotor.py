# coding=utf-8
'''
Copyright 2015 Diego Ariel Capeletti

Este archivo es parte del proyecto (((Dosificador alimenticio libre | Free food dispenser))).
Este archivo ha sido desarrollado para el control digital del servo que comanda la compuerta del
dispenser.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

'''
'''
Created on 07/10/2015

@author: diegoariel
'''

import threading
import time
import RPi.GPIO as GPIO    #Importamos la libreria RPi.GPIO
from observable import Observable
from composite import Compuesto

#PIN_SERVO=2 #Es el pin de señal del servo conectada al raspberry
ESTADO_SERVO=["abierto", "cerrado", "ciclando", "manual", "desconocido"]

'''
Esta clase es la encargada de cambiar el estado del servo o de obtener su estado.
El programa de esta clase se ejecuta como un hilo separado del programa principal 
ya que aquí se pueden hacer operaciones varias mientras que el programa principal
tiene que realizar y atender a otras. Es decir, necesitamos de los thread para lograr 
el paralelismo de las tareas.
'''

class Servo(threading.Thread, Observable, Compuesto):
    def __init__(self, pin_servo):
        threading.Thread.__init__(self)
        Observable.__init__(self)
        self.daemon = True  # OK for main to exit even if instance is still running #permite salir aunque el main se este ejecutando...
        self.paused = True  # Comienzo en pausa
        self.pin_servo = pin_servo #Es el pin de señal del servo conectada al microcontrolador
        self.__parent=0 #Contenedor padre del objeto
        self.state = threading.Condition() #Devuelve un objeto de condición variable. Permite que uno o mas hilos esperen hasta que sean notificados...
        self.inicializar_servo()

    def run(self):
        self.resume() #Al llamar a start(), el hilo se pone en marcha
        while True:
            with self.state:
                if self.paused: #Se evalua el estado del hilo, si esta pausado, lo pongo en espera...
                    self.state.wait() # block until notified #bloqeuar hasta que se notifica
            # do stuff #hacer cosas
            #Las operaciones a ejecutar se encuentran desde aquí.
            time.sleep(0.1) #Tiempo de retaro del hilo
            
            if self.get_estado_servo() != self.nuevo_estado:
                if self.nuevo_estado == ESTADO_SERVO[0]: #Estado abierto
                    self.pwm.ChangeDutyCycle(10) #Enviamos un pulso del 10.5% para girar el servo hacia la derecha
                    #self.pos_servo = 10.5
                    self.estado_servo = self.nuevo_estado
                    self.notify_observers("<estadoServo>" + str(10) + "</estadoServo>") #Es el estado de ChangeDutyCycle
                    print "Cambiando estado abierto"
                elif self.nuevo_estado == ESTADO_SERVO[1]: #Estado cerrado
                    self.pwm.ChangeDutyCycle(3)    #Enviamos un pulso del 4.5% para girar el servo hacia la izquierda
                    #self.pos_servo=4.5
                    self.nueva_posicion=0
                    self.estado_servo = self.nuevo_estado
                    self.notify_observers("<estadoServo>" + str(5) + "</estadoServo>") #Es el estado de ChangeDutyCycle
                    print "Cambiando estado cerrado"
                elif self.nuevo_estado == ESTADO_SERVO[2]: #El estado de ciclado no cambia el nuevo_estado para que así, se mantenga el ciclo 
                    self.pwm.ChangeDutyCycle(5)    #Enviamos un pulso del 4.5% para girar el servo hacia la izquierda
                    time.sleep(0.5)           #pausa de medio segundo
                    self.pwm.ChangeDutyCycle(10)   #Enviamos un pulso del 10.5% para girar el servo hacia la derecha
                    time.sleep(0.5)           #pausa de medio segundo
                    self.pwm.ChangeDutyCycle(7.5)    #Enviamos un pulso del 7.5% para centrar el servo de nuevo
                    time.sleep(0.5)           #pausa de medio segundo
                elif self.nuevo_estado == ESTADO_SERVO[3] and self.pos_servo != self.nueva_posicion:
                    self.pwm.ChangeDutyCycle(self.pos_servo)
                    self.nueva_posicion = self.pos_servo
                    self.estado_servo = ESTADO_SERVO[4]
                    self.notify_observers("<estadoServo>" + str(self.pos_servo) + "</estadoServo>") #Es el estado de ChangeDutyCycle
                    print "Cambiando estado manual: " + str(self.pos_servo)
                        


    def resume(self):
        #Pone en marcha la ejecución del thread
        with self.state:
            self.paused = False
            self.state.notify()  # unblock self if waiting #desbloquear auto esperando

    def pause(self):
        #Para la ejecución del thread
        with self.state:
            self.paused = True  # make self block and wait #El hilo deja de ejecutarse, entra en estado de espera..
            
    def inicializar_servo(self):
        GPIO.setmode(GPIO.BCM)   #Ponemos la Raspberry en modo BCM
        GPIO.setup(self.pin_servo,GPIO.OUT)    #Ponemos el pin 18 como salida
        pwm = GPIO.PWM(self.pin_servo,50)        #Ponemos el pin 2 en modo PWM y enviamos 50 pulsos por segundo
        pwm.start(3)               #Enviamos un pulso del 5 para cerrar el servo
        self.pwm=pwm
        self.nuevo_estado = ESTADO_SERVO[4] #Desconocido
        self.estado_servo = self.nuevo_estado
        self.nueva_posicion=0 #Variable auxiliar para control manual
        #self.cerrar() #Arranco el hilo en estado cerrado
        
    
    def abrir(self):
        #self.pwm.ChangeDutyCycle(10.5) #Enviamos un pulso del 10.5% para girar el servo hacia la derecha
        #self.pos_servo = 10.5
        self.nuevo_estado=ESTADO_SERVO[0]
        #self.pause() #Pauso el thread
        
    def cerrar(self):
        #self.pwm.ChangeDutyCycle(4.5)    #Enviamos un pulso del 4.5% para girar el servo hacia la izquierda
        #self.pos_servo=4.5
        self.nuevo_estado=ESTADO_SERVO[1]
        
    def ciclar(self):
        self.nuevo_estado=ESTADO_SERVO[2]
        
    def set_posicion_servo(self, posicion):
        #Establece una posición específica del motor.
        self.pos_servo = posicion
        self.nuevo_estado = ESTADO_SERVO[3]
        
    def detener_y_borrar(self):
        #Detiene el servo y limpiamos la configuración de pines GPIO
        self.pause()
        self.pwm.stop()
        GPIO.cleanup()
        
    def get_posicion_servo(self):
        #Retorna el valor de la posición del servo
        return self.pos_servo
    
    def get_estado_servo(self):
        return self.estado_servo
    
    def set_estado_servo(self, ESTADO_SERVO):
        self.estado_servo=ESTADO_SERVO
        
    def get_pin_servo_motor(self):
        return self.pin_servo
    
    def setParent(self, Compuesto):
        self.__parent = Compuesto
    
    def getParent(self):
        return self.__parent
    
    
    
    
    
class MotorSinThreads(Observable, Compuesto):
    #¿Es necesario que la clase motor sea un thread?
    def __init__(self, pin_servo):
        Observable.__init__(self)
        self.pin_servo=pin_servo
        self.arrancado = False
        
    def cerrar(self):
        #self.pwm.ChangeDutyCycle(3) #posicion cerrado
        self.angulo(3)
        
        
    def abrir(self):
        self.angulo(10)
        
        
    def set_posicion_servo(self, pos):
        if pos == 3: #Apagar
            self.cerrar()
        else:
            self.angulo(pos)
    
        
    def angulo(self, pos):
        #print "Arrancando"
        if self.arrancado == False:        
            self.__arrancar_motor()
        self.pwm.ChangeDutyCycle(pos)
        self.notify_observers("<estadoServo>" + str(pos) + "</estadoServo>")
        if pos==3: #parar motor
            time.sleep(1) #Espero 2 segundos    
            self.__parar_motor()
    

    def __parar_motor(self):
        self.pwm.stop()
        self.arrancado = False

    def __arrancar_motor(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.pin_servo, GPIO.OUT)
        self.pwm = GPIO.PWM(self.pin_servo, 50)
        self.pwm.start(5) #posicion cerrado
        self.arrancado = True
        
        
    def detener_y_borrar(self):
        #Detiene el servo y limpiamos la configuración de pines GPIO
        self.pwm.stop()
        GPIO.cleanup()
        
        
    def get_pin_servo_motor(self):
        return self.pin_servo
    
    def setParent(self, Compuesto):
        self.__parent = Compuesto
    
    def getParent(self):
        return self.__parent
    
    
    
    
    
class MotorConThreads(threading.Thread, Observable, Compuesto):
    #¿Es necesario que la clase motor sea un thread?
    def __init__(self, pin_servo):
        threading.Thread.__init__(self)
        Observable.__init__(self)
        self.daemon = True  # OK for main to exit even if instance is still running #permite salir aunque el main se este ejecutando...
        self.paused = True  # Comienzo en pausa
        self.__estados=["abrir", "cerrar", "manual", "desconocido"]
        self.__estado_actual=[]
        self.estado_servo = self.__estado_actual 
        self.__parent=0 #Contenedor padre del objeto
        self.state = threading.Condition() #Devuelve un objeto de condición variable. Permite que uno o mas hilos esperen hasta que sean notificados...

        self.pin_servo=pin_servo
        self.__pos=3#Cerrado
        self.__nueva_posicion=0
        self.arrancado = False
        
        
    def run(self):
        self.resume() #Al llamar a start(), el hilo se pone en marcha
        while True:
            with self.state:
                if self.paused: #Se evalua el estado del hilo, si esta pausado, lo pongo en espera...
                    self.state.wait() #bloquea hasta que se notifica con resume
            #hacer cosas
            #Las operaciones a ejecutar se encuentran desde aquí.
            time.sleep(0.1) #Tiempo de retaro del hilo
            
            if self.get_estado_servo() != self.__estado_actual:
                if self.__estado_actual==self.__estados[0]:#abrir
                    if self.arrancado == False:        
                            self.__arrancar_motor()
                    print "Cambiando a estado abierto..."
                    self.pwm.ChangeDutyCycle(self.__pos)
                    self.set_estado_servo(self.__estado_actual)
                    self.__nueva_posicion = self.__pos
                    self.notify_observers("<estadoServo>" + str(self.__pos) + "</estadoServo>")
                if self.__estado_actual == self.__estados[1]: #cerrar
                    if self.arrancado == False:        
                        self.__arrancar_motor()
                    print "Cambiando a estado cerrado..."
                    self.pwm.ChangeDutyCycle(self.__pos)
                    self.set_estado_servo(self.__estado_actual)
                    self.__nueva_posicion = self.__pos
                    self.notify_observers("<estadoServo>" + str(self.__pos) + "</estadoServo>")
                    time.sleep(1) #Espero 1 segundo para el cierre    
                    self.__parar_motor()
                if self.__estado_actual==self.__estados[2] and self.__pos != self.__nueva_posicion:#angulo espeficido
                    print "Cambiando a estado manual " + str(self.__pos)
                    if self.arrancado == False:        
                            self.__arrancar_motor()
                    self.__nueva_posicion = self.__pos
                    self.pwm.ChangeDutyCycle(self.__pos)
                    self.set_estado_servo(self.__estados[3])
                    self.notify_observers("<estadoServo>" + str(self.__pos) + "</estadoServo>")
                
            
    
    def resume(self):
        #Pone en marcha la ejecución del thread
        with self.state:
            self.paused = False
            self.state.notify()  # unblock self if waiting #desbloquear auto esperando

    def pause(self):
        #Para la ejecución del thread
        with self.state:
            self.paused = True  # make self block and wait #El hilo deja de ejecutarse, entra en estado de espera..

     
    def abrir(self):
        #self.angulo(10)
        self.__pos=10  
        self.__estado_actual=self.__estados[0] 
       
        
    def cerrar(self):
        #self.pwm.ChangeDutyCycle(3) #posicion cerrado
        #self.angulo(3)
        self.__pos=3 #cerrar
        self.__estado_actual=self.__estados[1]
             
        
    def set_posicion_servo(self, pos):
        print "    Nueva posicion ", pos
        if pos == 3: #Apagar
            self.cerrar()
        else:
            #self.angulo(pos)
            self.__pos = pos
            self.set_estado_servo(self.__estados[3])
            self.__estado_actual=self.__estados[2] #angulo espefico
    
    '''    
    def angulo(self, pos):
        #print "Arrancando"
        if self.arrancado == False:        
            self.__arrancar_motor()
        self.pwm.ChangeDutyCycle(pos)
        self.notify_observers("<estadoServo>" + str(pos) + "</estadoServo>")
        if pos==3: #parar motor
            time.sleep(1) #Espero 2 segundos    
            self.__parar_motor()
    '''

    def __parar_motor(self):
        self.pwm.stop()
        self.arrancado = False

    def __arrancar_motor(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.pin_servo, GPIO.OUT)
        self.pwm = GPIO.PWM(self.pin_servo, 50)
        self.pwm.start(3) #posicion cerrado
        self.arrancado = True
        
        
    def detener_y_borrar(self):
        #Detiene el servo y limpiamos la configuración de pines GPIO
        self.pause()
        self.pwm.stop()
        GPIO.cleanup()
        
        
    def get_pin_servo_motor(self):
        return self.pin_servo
    
    
    def get_estado_servo(self):
        return self.estado_servo
    
    def set_estado_servo(self, ESTADO_SERVO):
        self.estado_servo=ESTADO_SERVO
    
    def setParent(self, Compuesto):
        self.__parent = Compuesto
    
    def getParent(self):
        return self.__parent
    
    

        