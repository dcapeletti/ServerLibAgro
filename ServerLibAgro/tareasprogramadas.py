# coding=utf-8
'''
Copyright © 2015-2017 Diego Ariel Capeletti

Este archivo es parte del proyecto LibAgro Server (Dosificador T4C).

LibAgro Server is libre software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LibAgro Server is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LibAgro Server.  If not, see <http://www.gnu.org/licenses/>.
'''

from observable import Observable
from service_db import ServiceBD
'''
Created on 17/12/2015

@author: diegoariel
'''

'''Conjunto de clases que realizan cálculos sobre los tiempos de las tareas que la 
máquina debe realizar. Conociendo los tiempos de reacción, la máquina puede realizar tareas de
simulación automáticamente con presición de 2 nanosegundos...
La clase carga las operciones día a día configuradas en la BD. Cuando cambia de día
carga las nuevas configuraciones. Por cada cierto tiempo se revizan si no hay nuevas 
configuraciones en la BD. Cuando una configuración de tiempo coincide con el reloj del 
sistema, se dispara la tarea programada, por el tiempo indicado...
'''

import threading
import time, datetime
import MySQLdb
from observable import Observable

class GestorTareaProgramada(Observable, threading.Thread):
    def __init__(self, Compuerta, Maquina):
        #El contructor recibe un objeto de la clase Compuerta a la que envía el mensaje cuando hay que hacer algo...
        #Esta clase podría reportar al observador que se esta cumpliendo la tarea programada...
        threading.Thread.__init__(self)
        Observable.__init__(self)
        self.daemon=True
        self.paused=True
        self.state=threading.Condition()
        
        self.__compuerta = Compuerta #Es la compuerta a la que se le da la instrucción...
        self.__maquina = Maquina #Solo para obtener la conexion a la BD
        self.__contador=0 #Con un contador de segundos, podemos hacer que ciertas tareas, se ejecuten al minuto...
        
        self.__fechaActual = datetime.datetime.now() #Fecha actual...
        self.__horaActual =  datetime.datetime.now() #Hora actualizada
        self.__tareas_compuertas=[] #Cola en la que se guardan las tareas que deben abrirse las compuertas...
        self.__tarea_actual = None
        self.observer=0
        #self.cargarTareas()
        self.cargarTareasDelDia()
    

    def run(self):
        self.resume() #Al hacer start() se da inicio al thread
        while True:
            with self.state:
                if self.paused: #Se evalua el estado del hilo, si esta pausado, lo pongo en espera...
                    self.state.wait() # block until notified #bloqeuar hasta que se notifica
            try:
                self.__horaActual=datetime.datetime(datetime.datetime.now().year, 
                                        datetime.datetime.now().month, 
                                        datetime.datetime.now().day, 
                                        datetime.datetime.now().hour, 
                                        datetime.datetime.now().minute, 
                                        datetime.datetime.now().second)
                if self.__fechaActual.day != self.__horaActual.day: #Si el dia es distinto actualizo la fecha
                    self.__fechaActual = datetime.datetime.now()
                    #self.cargarTareas() #Cargo las tareas del dia...
                    self.cargarTareasDelDia()
                if len(self.__tareas_compuertas)!=0: #Verifico si hay una proxima tarea configurada en la lista...
                    if self.__tareas_compuertas[0].getInicio() == self.__horaActual: #Verifico cuando llega la hora indicada...
                        #self.ejecutar()
                        print "Los horarios coinciden:", self.__tareas_compuertas[0].getInicio(), self.__horaActual
                        if TareaAbrirServo.__name__ in str(self.__tareas_compuertas[0].__class__): #Verifico el tipo de tarea...
                            tarea = self.__tareas_compuertas[0]
                            self.setTareaActual(tarea)
                            print "Ejecutando tarea ", tarea.getInicio()
                            self.notify_observers("Ejecutando tarea: "+ str(tarea.getidTarea()) +", "+ str(self.__compuerta.getIdCompuerta()) +"\n") #Le doy aviso a los clientes...
                            self.__compuerta.getMotor().set_posicion_servo(int(tarea.getPosicion()))
                            #se podria hacer un Boton.pause() para que no responda mientras se ejecuta la tarea?
                            time.sleep(float(tarea.getDuracion()))
                            self.__compuerta.getMotor().cerrar() #Señal de cierre
                            print "Tarea finalizada..."
                            #cur = self.__maquina.getServiceBd().getBd().cursor()
                            #cur.execute("Update CONFIG_APERTURAS_COMPUERTAS set Ejecutado=1 Where idCONFIG_APERTURAS_COMPUERTAS=" + str(tarea.getidTarea()))
                            self.notify_observers("<tareaprogramadaejecutada>"+str(tarea.getidTarea())+"</tareaprogramadaejecutada>")
                            del self.__tareas_compuertas[0] #Elimino la última tarea luego de ejecutarse
            except Exception as error:
                if error[0] == 2013: #(2013, 'Lost connection to MySQL server during query')
                    print "No se puede enviar la consulta al servidor de datos...", error
                else:
                    print "Error en GestorTareaProgramada...", error
                
            time.sleep(1)
    
    def resume(self):
        #Pone en marcha la ejecución del thread
        with self.state:
            self.paused = False
            self.state.notify()  # unblock self if waiting #desbloquear auto esperando

        
    def pause(self):
        #Para la ejecución del thread
        with self.state:
            self.paused = True  #El hilo deja de ejecutarse, entra en estado de espera..
            
        
    def cargarTareas(self):
        #Buscar TODAS las tareas que correspondan al día de hoy, que sean mayor a la hora actual y que estén ordenadas por hora...
        print "Cargando las tareas...", self.__fechaActual
        del self.__tareas_compuertas[0:] #Elimino TODAS las tareas que se encuentren en la lista...
        bd = MySQLdb.connect(host=self.__maquina.getServiceBd().serverBD(), user=self.__maquina.getServiceBd().usuarioBD(), passwd=self.__maquina.getServiceBd().passwordBD(), db=self.__maquina.getServiceBd().nombreBD())
        cur = bd.cursor()
        try:
            print "Hora actual", str(self.__horaActual.hour) + ":" + str(self.__horaActual.minute) + ":" + str(self.__horaActual.second)
            cur.execute("SELECT CONFIG_APERTURAS_COMPUERTAS.Id_Compuerta, CONFIG_APERTURAS_COMPUERTAS.Dia, "
                            " CONFIG_APERTURAS_COMPUERTAS.Hora, CONFIG_APERTURAS_COMPUERTAS.Config_Kg, CONFIG_PARAMETROS_KILOS_ALIMENTOS.Kg, "+
                            " CONFIG_PARAMETROS_KILOS_ALIMENTOS.Apertura, CONFIG_PARAMETROS_KILOS_ALIMENTOS.Cierre, CONFIG_APERTURAS_COMPUERTAS.idCONFIG_APERTURAS_COMPUERTAS as Id_Tarea "+
                            " FROM CONFIG_APERTURAS_COMPUERTAS inner join CONFIG_PARAMETROS_KILOS_ALIMENTOS on "+ 
                            " CONFIG_APERTURAS_COMPUERTAS.Config_Kg=CONFIG_PARAMETROS_KILOS_ALIMENTOS.idCONFIG_PARAMETROS_KILOS"+
                            " Where CONFIG_APERTURAS_COMPUERTAS.Id_Compuerta=" + str(self.__compuerta.getIdCompuerta()) + 
                            " and CONFIG_APERTURAS_COMPUERTAS.Dia='" + str(self.__fechaActual.year) + "-" + str(self.__fechaActual.month) + "-" + str(self.__fechaActual.day) + "'" +
                            " and CONFIG_APERTURAS_COMPUERTAS.Hora > '"+ str(self.__horaActual.hour) + ":" + str(self.__horaActual.minute) + ":" + str(self.__horaActual.second) + "'"+
                            " and CONFIG_APERTURAS_COMPUERTAS.Ejecutado=0 " +
                            " group by CONFIG_APERTURAS_COMPUERTAS.Dia, CONFIG_APERTURAS_COMPUERTAS.Config_Kg, CONFIG_APERTURAS_COMPUERTAS.Hora "+
                            " order by CONFIG_APERTURAS_COMPUERTAS.Hora")
                
            filas = cur.fetchall()
            #print cur.rowcount, " registros encontrados.."    
            for fila in filas:
                tarea = TareaAbrirServo()
                hora = str(fila[2]) #La hora es de tipo datetime.timedelta
                if len(str(hora))==7: #Es un horario entre las 0 y las 9, le agrego el 0 delante para que quede 05, 09, 06, etc
                    hora = "0"+ str(hora)
                                        
                tarea.setInicio(datetime.datetime(fila[1].year, fila[1].month, fila[1].day, int(hora[0:2]), int(hora[3:5]), int(hora[6:8])))
                tarea.setDuracion(float(fila[6])) #Cierre
                tarea.setPosicion(float(fila[5])) #Diración
                tarea.setIdTarea(int(fila[7]))
                self.__tareas_compuertas.append(tarea)
                
                print "    Hay", len(self.__tareas_compuertas) , "tareas para ejecutar en el día", self.__fechaActual
            bd.close()
        except Exception as error:
            print "Error en " + str(self.__class__) + ":", error
            
    
    def cargarTareasDelDia(self):
        '''Busco las tareas del dia por consulta. Busco las modificaciones.
        '''
        bd = MySQLdb.connect(host=self.__maquina.getServiceBd().serverBD(), user=self.__maquina.getServiceBd().usuarioBD(), passwd=self.__maquina.getServiceBd().passwordBD(), db=self.__maquina.getServiceBd().nombreBD())
        tiempo_inicio = datetime.datetime.now()
        print "Cargando las tareas del dia", datetime.date.today(), 'para la compuerta', self.__compuerta.getIdCompuerta()
        del self.__tareas_compuertas[0:] #Elimino TODAS las tareas que se encuentren en la lista...   
        cursor = bd.cursor()
        cursorProc = bd.cursor()
        cursor.execute("delete from BOM_TAREAS_PROGRAMADAS Where Fecha = %s and Compuerta=%s", (datetime.date.today(), self.__compuerta.getIdCompuerta(),))
        cursor.execute("SELECT * FROM ListaTareasProgramadasFechaActual where "+
            "ListaTareasProgramadasFechaActual.Id_Compuerta=%s order by Hora_Ejecucion", (self.__compuerta.getIdCompuerta(),))
        resultado_tareas_dia = cursor.fetchall()
        
        id_config_tareas=0
        contador_tareas =0
        cursorProc.callproc('CONSULTA_MODIFICACION_TAREAS', [str(datetime.date.today())]) #llama al procedimiento que crea las tablas temporales
        for res in resultado_tareas_dia:
            if id_config_tareas != res[0]: #Filtro cada tarea y cargo su configuracion
                cur_tarea_sin_moficicaciones = bd.cursor()
                tarea = TareaAbrirServo()
                tarea.setCompuerta(self.__compuerta.getIdCompuerta())
                hora = str(res[5]) #La hora es de tipo datetime.timedelta
                if len(str(hora))==7: #Es un horario entre las 0 y las 9, le agrego el 0 delante para que quede 05, 09, 06, etc
                    hora = "0"+ str(hora)
                print "   Tienes la tarea", res[0], 'con hora de ejecucion', hora
                tarea.setIdTarea(int(res[0]))    
                tarea.setInicio(datetime.datetime(datetime.date.today().year, datetime.date.today().month, datetime.date.today().day, int(hora[0:2]), int(hora[3:5]), int(hora[6:8])))
                tarea.setConfig_Kg(res[2])
                cur_tarea_sin_moficicaciones.execute("Select * from CONFIG_PARAMETROS_KILOS_ALIMENTOS Where idCONFIG_PARAMETROS_KILOS=%s", (res[2],))
                res_cur_tarea_sin_moficicaciones = cur_tarea_sin_moficicaciones.fetchall()
                for val_res_cur_tarea_sin_moficicaciones in res_cur_tarea_sin_moficicaciones: #Cargo la config_kg de la tarea original
                    tarea.setPosicion(val_res_cur_tarea_sin_moficicaciones[4]) 
                    tarea.setDuracion(val_res_cur_tarea_sin_moficicaciones[3])
                cursor.execute('Select * from ListaTareasModificadas Where idCONFIG_TAREAS_PROGRAMADA=%s', (res[0],))
                resultado_modificaciones = cursor.fetchall()
                for res_modificaciones in resultado_modificaciones: #Busco las modificaciones para la tarea
                    print '        Tarea', res_modificaciones[0], 'con modificacion', res_modificaciones[1]
                    cursorModificacion = bd.cursor()
                    cursorModificacion.execute("Select MODIFICACION_INSTANCIAS_TAREAS.Hora_Ejecucion, "+
                    "CONFIG_PARAMETROS_KILOS_ALIMENTOS.Cierre, CONFIG_PARAMETROS_KILOS_ALIMENTOS.Apertura, "+
                    "MODIFICACION_INSTANCIAS_TAREAS.Config_Kg "+                           
                    "from ListaTareasModificadas INNER JOIN MODIFICACION_INSTANCIAS_TAREAS "+
                    "on ListaTareasModificadas.IdModTarea = MODIFICACION_INSTANCIAS_TAREAS.idMODIFICACION_INSTANCIA_TAREA " +
                    "inner join CONFIG_PARAMETROS_KILOS_ALIMENTOS ON MODIFICACION_INSTANCIAS_TAREAS.Config_Kg = "+
                    "CONFIG_PARAMETROS_KILOS_ALIMENTOS.idCONFIG_PARAMETROS_KILOS "+ 
                    "Where ListaTareasModificadas.idCONFIG_TAREAS_PROGRAMADA=%s "+
                    "order by MODIFICACION_INSTANCIAS_TAREAS.Hora_Ejecucion", (res_modificaciones[0],))
                    res_consulta_modificaciones = cursorModificacion.fetchall()
                    for val in res_consulta_modificaciones: #Sobreescribo las modificiones
                        hora = str(val[0]) #La hora es de tipo datetime.timedelta
                        if len(str(hora))==7: #Es un horario entre las 0 y las 9, le agrego el 0 delante para que quede 05, 09, 06, etc
                            hora = "0"+ str(hora)
                        #print hora
                        tarea.setInicio(datetime.datetime(datetime.date.today().year, datetime.date.today().month, datetime.date.today().day, int(hora[0:2]), int(hora[3:5]), int(hora[6:8])))
                        tarea.setPosicion(val[2])
                        tarea.setDuracion(val[1])
                        tarea.setConfig_Kg(val[3])
                if tarea.getInicio() > datetime.datetime.now(): #Agrego a la lista de tareas a ejecutar solo quellas tareas cuya hora de inicio es mayor a la hora actual
                    self.__tareas_compuertas.append(tarea)
                #guardo todas las tareas del día en la bd, de manera sincrona o asíncrona???
                cursorGuardarTareas = bd.cursor()
                cursorGuardarTareas.execute("Insert into BOM_TAREAS_PROGRAMADAS (Id_Tarea, Compuerta, Fecha, " +
                                            "Hora_Ejecucion, Config_Kg) " +
                                            "values (%s, %s, %s, %s, %s)", (tarea.getidTarea(), tarea.getCompuerta(), tarea.getInicio().strftime("%Y-%m-%d"), 
                                                                    tarea.getInicio().strftime("%H:%M:%S"), tarea.getConfig_Kg(), ))
                cursorGuardarTareas.connection.commit()
                
                contador_tareas +=1
            
            id_config_tareas = res[0]
        
        tiempo_fin = datetime.datetime.now()
        print tiempo_fin - tiempo_inicio    
        print 'TOTAL:', contador_tareas, "tareas para el dia", str(datetime.date.today())
        print "Solo", len(self.__tareas_compuertas), 'tarea/s sera ejecutada por diferencia horaria'
        print "\n"
        if len(self.__tareas_compuertas) > 0: 
            print "LISTA DE TAREAS A EJECUTAR PARA EL", datetime.date.today()
        for tarea_comp in self.__tareas_compuertas:
            print 'Tarea', tarea_comp.getidTarea(), 'hora de ejecucion' , tarea_comp.getInicio(), 'posicion', tarea_comp.getPosicion() ,'duracion', tarea_comp.getDuracion(), 'segundos'

        
        
    def getCompuerta(self):
        return self.__compuerta   
    
    def setTareaActual(self, tarea):
        self.__tarea_actual = tarea
        
        
    def getTareaActual(self):
        return self.__tarea_actual
    




class TareaAbrirServo():
    def __init__(self):
        self.__inicio=0 #Establece la hora con presiion de segundo a la que se debe dar inicio...
        self.__duracion=0 #Se establece la duracion con presicion de 2 nanosegundos...
        self.__posicion=0 #Se establece a que posicion debe llehar el servo...
        self.__idTarea=0 #Identificador de la tarea programada...
        self.__config_kg =0 #identificador del id de la tabla CONFIG_PARAMETROS_KILOS_ALIMENTOS
        self.__compuerta =0 #identificador de la compuerta
    
    def setInicio(self, valor):
        self.__inicio = valor
        
    def getInicio(self):
        return self.__inicio
        
    def setDuracion(self, valor):
        self.__duracion = valor
    
    def getDuracion(self):
        return self.__duracion
        
    def setPosicion(self, valor):
        self.__posicion = valor
        
    def getPosicion(self):
        return self.__posicion
    
    def setIdTarea(self, valor):
        self.__idTarea=valor
        
    def getidTarea(self):
        return self.__idTarea
    
    def setConfig_Kg(self, valor):
        self.__config_kg = valor
        
    def getConfig_Kg(self):
        return self.__config_kg    
    
    def setCompuerta(self, valor):
        self.__compuerta = valor
        
    def getCompuerta(self):
        return self.__compuerta   
    