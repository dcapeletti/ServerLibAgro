# coding=utf-8
'''
Copyright 2015 Diego Ariel Capeletti

Este archivo es parte del proyecto (((Dosificador alimenticio libre | Free food dispenser))).

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''
'''
Created on 08/10/2015

@author: diegoariel
'''

import threading
import time
import RPi.GPIO as GPIO    #Importamos la libreria RPi.GPIO
from observable import Observable
from composite import Compuesto

class Boton(threading.Thread, Observable, Compuesto):
    def __init__(self, pin_boton, mensaje):
        threading.Thread.__init__(self)
        Observable.__init__(self)
        self.daemon = True  # OK for main to exit even if instance is still running #permite salir aunque el main se este ejecutando...
        self.paused = True  # Comienzo en pausa
        self.pin_boton = pin_boton #Id del pin del micro al que escucha al sensor..
        self.__parent=0 #Padre del objeto...
        self.mensaje_notificacion = mensaje #Es el mensaje que se envía al mediador ante eventos en el sensor.
        self.tiempo_ejecucion_thread = 0.5 #Establece la frecuencia de actualziación del thread que escucha el sensor. Por defecto medio segundo.
        self.state = threading.Condition() #Devuelve un objeto de condición variable. Permite que uno o mas hilos esperen hasta que sean notificados...
        self.inicializar_boton()
        
    def run(self):
        self.resume()
        while True:
            with self.state:
                if self.paused: #Se evalua el estado del hilo, si esta pausado, lo pongo en espera...
                    self.state.wait() # block until notified #bloqeuar hasta que se notifica
            #try:
            #while True:
            self.estado_actual = GPIO.input(self.pin_boton) #Obtengo la lectura del boton
            if self.estado_actual != self.estado_anterior: #Solamente reporto ante un cambio de estado del boton...
                #print('Cambio de estado en boton')
                self.estado_anterior=self.estado_actual
                self.notify_observers("<" + self.mensaje_notificacion + ">"+ str(self.estado_actual) +"</"+ self.mensaje_notificacion +">\n") #Notificacion al observador
            time.sleep(self.tiempo_ejecucion_thread)
            #except:
                #print "Ha ocurrido un error en el control del boton..."
            
        
    def resume(self):
        #Pone en marcha la ejecución del thread
        with self.state:
            self.paused = False
            self.state.notify()  # unblock self if waiting #desbloquear auto esperando

    def pause(self):
        #Para la ejecución del thread
        with self.state:
            self.paused = True  # make self block and wait #El hilo deja de ejecutarse, entra en estado de espera..
            
    def inicializar_boton(self):
        #Los estados arrancan igualados
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.pin_boton, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        self.estado_actual=1
        self.estado_anterior=1
        
    def detener_y_borrar(self):
        #Detiene la lectura del botón y limpia la configuración del GPIO
        self.pause()
        GPIO.cleanup()
            
    def get_estado_boton(self):
        return self.estado_actual
    
    
    def setEstadoInicialBoton(self, valor):
        self.estado_anterior=valor
    
    def setPin_Boton(self, pin_boton):
        self.pin_boton = pin_boton
        
    
    def getPin_Boton(self):
        return self.pin_boton
    
    
    def setFrecuencia_actualizacion_thread(self, valor):
        self.tiempo_ejecucion_thread = valor
        
    def setParent(self, Compuesto):
        self.__parent = Compuesto
    
    def getParent(self):
        return self.__parent
    
    def setMensajeNotificacion(self, valor):
        self.mensaje_notificacion=valor
    












        
