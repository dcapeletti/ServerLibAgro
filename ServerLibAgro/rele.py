# coding=utf-8
'''
Copyright 2015 Diego Ariel Capeletti

Este archivo es parte del proyecto (((Dosificador alimenticio libre | Free food dispenser))).

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''

'''
Created on 09/10/2015

@author: diegoariel
'''
import RPi.GPIO as GPIO
import time
import threading
from observable import Observable
from composite import Compuesto


class Rele(threading.Thread, Observable, Compuesto):
    def __init__(self, pin_rele, mensaje):
        threading.Thread.__init__(self)
        Observable.__init__(self)
        self.pin_rele = pin_rele #Pin del microcontrolador conectado al rele.
        self.__parent=0 #Contenedor padre del objeto
        self.mensaje_notificacion = mensaje #Mensaje que se envía al mediador ante eventos del rele.
        self.daemon=True
        self.paused=True
        self.state=threading.Condition()
        self.inicializar_rele()
    
    def run(self):
        #self.resume()
        while True:
            with self.state:
                if self.paused: #Se evalua el estado del hilo, si esta pausado, lo pongo en espera...
                    self.state.wait() # block until notified #bloqeuar hasta que se notifica
            #Hacer algo
            print "Cambiando estado de relé"
            if self.estado == 1:
                GPIO.output(self.pin_rele, GPIO.HIGH)
            else:
                GPIO.output(self.pin_rele, GPIO.LOW)
            #self.notify_observers("Estado de rele: " + str(self.estado))
            #time.sleep(0.1)
            self.pause()

        
        
    def resume(self):
        with self.state:
            self.paused = False
            self.state.notify()  # unblock self if waiting #desbloquear auto esperando
            
    def pause(self):
        #Para la ejecución del thread
        with self.state:
            self.paused = True
            
    
    def inicializar_rele(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.pin_rele, GPIO.OUT)
        self.estado=0
        
        
    def activar(self):
        self.estado=1
        self.resume()
        
        
    def desactivar(self):   
        self.estado=0 
        self.resume()
        
    def detener_y_borrar(self):
        GPIO.cleanup()
        
    def setPin_rele(self, pin_rele):
        self.pin_rele = pin_rele
        
    def getPin_rele(self):
        return self.pin_rele
    
    def setParent(self, Compuesto):
        self.__parent = Compuesto
    
    def getParent(self):
        return self.__parent
        